package com.example.gebruiker.btbdi;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.gebruiker.btbdi.Database.DatabaseHelper;
import java.util.HashMap;
import java.util.Map;

public class Introductie extends AppCompatActivity {

    TextView textArea;
    String deel;
    String week;

    String getUrl = "http://145.101.80.72/btbdi/showtext.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introductie);

        DatabaseHelper.getHelper(this);

        Bundle extras = getIntent().getExtras();
        week = extras.getString("week");
        deel = extras.getString("deel");

        TextView label = (TextView) findViewById(R.id.label);

        label.setText(deel);

        RequestQueue requestQueue;

        textArea = (TextView) findViewById(R.id.textArea);

        requestQueue = Volley.newRequestQueue(getApplicationContext());

                StringRequest stringRequest = new StringRequest(Request.Method.POST, getUrl, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            String text = response;
                            textArea.append(text);
                        }catch(Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        textArea.setText("Error: \n\n" + error.toString());
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams(){
                        Map<String, String> params = new HashMap<>();
                        params.put("week", week);
                        params.put("deel", deel);
                        return params;
                    }
                }; requestQueue.add(stringRequest);
        }



}

