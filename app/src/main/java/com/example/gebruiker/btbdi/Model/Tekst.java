package com.example.gebruiker.btbdi.Model;

/**
 * Created by Braz on 23/02/2017.
 */

public class Tekst {

    public String week;
    public String deel;
    public String tekst;

    public Tekst(String week, String deel, String tekst){
        this.week = week;
        this.deel = deel;
        this.tekst = tekst;
    }
}
