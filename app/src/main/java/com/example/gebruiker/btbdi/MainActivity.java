package com.example.gebruiker.btbdi;

import android.app.FragmentManager;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.gebruiker.btbdi.Database.DatabaseHelper;
import com.example.gebruiker.btbdi.Database.DatabaseInfo;
import com.example.gebruiker.btbdi.GSON.GsonRequest;
import com.example.gebruiker.btbdi.GSON.VolleyHelper;
import com.example.gebruiker.btbdi.Model.Tekst;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    String getUrl = "http://192.168.2.4/btbdi/showtext.php";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DatabaseHelper.getHelper(this);

        checkLoginStatus();
        checkSync();

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void checkSync(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean syncd = prefs.getBoolean("sync", false);

        if (!syncd){
            requestSubjects();
        }
        else {
            return;
        }
    }

    public void checkLoginStatus(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean signedIn = prefs.getBoolean("Islogin", false);

        if(!signedIn)
        {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
        else {
            Snackbar.make(findViewById(android.R.id.content), "Ingelogd!", Snackbar.LENGTH_LONG).show();
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        FragmentManager fragmentManager = getFragmentManager();

        if (id == R.id.nav_first_layout) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame
                            , new FirstFragment())
                    .commit();
        } else if (id == R.id.nav_second_layout) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame
                            , new SecondFragment())
                    .commit();
        } else if (id == R.id.sign_out) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            prefs.edit().putBoolean("Islogin", false).commit();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private void requestSubjects(){
        Type type = new TypeToken<List<Tekst>>(){}.getType();

        GsonRequest<List<Tekst>> request = new GsonRequest<>(getUrl,
                type, null, new Response.Listener<List<Tekst>>(){
            @Override
            public void onResponse(List<Tekst> response) {
                processRequestSucces(response);
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                System.out.println(error);
            }
        });
        VolleyHelper.getInstance(this).addToRequestQueue(request);
    }
    private void processRequestSucces(List<Tekst> subjects ){

        // putting all received classes in my database.
        DatabaseHelper dbHelper = new DatabaseHelper(this);
        for (Tekst txt : subjects) {
            ContentValues cv = new ContentValues();
            cv.put(DatabaseInfo.TextColumn.WEEK, txt.week);
            cv.put(DatabaseInfo.TextColumn.DEEL, txt.deel);
            cv.put(DatabaseInfo.TextColumn.TEKST, txt.tekst);

            dbHelper.insert(DatabaseInfo.TextTables.TEKST, null, cv);
        }

        Cursor rs = dbHelper.query(DatabaseInfo.TextTables.TEKST, new String[]{"*"}, null, null, null, null, null);
        rs.moveToFirst();   // kan leeg zijn en faalt dan
        DatabaseUtils.dumpCursor(rs);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.super.getBaseContext());
        prefs.edit().putBoolean("sync", true).commit();

    }
}
