package com.example.gebruiker.btbdi.Database;

/**
 * Created by Victor on 6-12-2016.
 */

public class DatabaseInfo {

    public class TextTables {
        public static final String TEKST = "tekst";   // NAAM VAN JE TABEL
    }

    public class TextColumn {
        public static final String WEEK = "week";	// VASTE WAARDES
        public static final String DEEL = "deel";	// NAAM VAN DE KOLOMMEN
        public static final String TEKST = "tekst";	// FINAL !
    }
}
