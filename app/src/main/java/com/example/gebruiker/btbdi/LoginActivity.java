package com.example.gebruiker.btbdi;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    RequestQueue requestQueue;
    EditText studentnr;
    EditText wachtwoord;

    String insertUrl = "http://145.101.80.72/btbdi/login.php";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Email TextField
        studentnr = (EditText) findViewById(R.id.studentNr);


        //wachtwoord TextField
        wachtwoord = (EditText) findViewById(R.id.password);

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        Log.d("Main activity gestart", "Main activity");


        //Sign Up
        TextView singUpLnk = (TextView) findViewById(R.id.singUp);

        singUpLnk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
            }
        });

        //Login button
        Button loginBtn = (Button) findViewById(R.id.singIn);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, insertUrl, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            if (response.trim().equals("true")) {

                                //http://stackoverflow.com/questions/22526950/how-to-check-if-current-user-is-logged-in-android

                                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.super.getBaseContext());
                                prefs.edit().putBoolean("Islogin", true).commit();

                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            } else {
                                startActivity(new Intent(LoginActivity.this, LoginActivity.class));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        final String studentNummer = studentnr.getText().toString();
                        final String password = wachtwoord.getText().toString();

                        Map<String, String> params = new HashMap<>();
                        params.put("studentnr", studentNummer);
                        params.put("wachtwoord", password);

                        return params;
                    }
                };
                requestQueue.add(stringRequest);
                finish();
            }
        });
    }
    @Override
    public void onBackPressed() {
    }
}