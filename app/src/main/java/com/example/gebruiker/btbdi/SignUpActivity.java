package com.example.gebruiker.btbdi;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

public class SignUpActivity extends AppCompatActivity {

    Button signInButton;
    EditText firstnameField;
    EditText lastNameField;
    EditText studentNumberField;
    EditText passwordField;
    RequestQueue requestQueue;

    String insertUrl = "http://145.101.80.72/btbdi/insert.php";


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        firstnameField = (EditText) findViewById(R.id.firstnameField);
        lastNameField = (EditText) findViewById(R.id.lastnameField);
        studentNumberField = (EditText) findViewById(R.id.studentNumberField);
        passwordField = (EditText) findViewById(R.id.studentPassword);
        requestQueue = Volley.newRequestQueue(getApplicationContext());

        signInButton = (Button) findViewById(R.id.singUpButton);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, insertUrl, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        alert("Account succesvol aangemaakt!");
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        alert("Account aanmaken mislukt!");
                    }
                }){
                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String> params = new HashMap<>();
                        params.put("voornaam",firstnameField.getText().toString());
                        params.put("achternaam",lastNameField.getText().toString());
                        params.put("studentnr",studentNumberField.getText().toString());
                        params.put("wachtwoord", passwordField.getText().toString());

                        return params;

                    }
                }; requestQueue.add(stringRequest);

            }

        });

    }
    public void alert(String message){

        Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG).show();

        Button login = (Button) findViewById(R.id.lgin);

        signInButton.setEnabled(false);
        signInButton.setVisibility(View.INVISIBLE);
        login.setEnabled(true);
        login.setVisibility(View.VISIBLE);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
            }
        });

    }
}