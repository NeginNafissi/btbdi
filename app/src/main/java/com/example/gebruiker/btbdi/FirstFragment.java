package com.example.gebruiker.btbdi;

import android.app.Fragment;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Gebruiker on 20-2-2017.
 */

public class FirstFragment extends Fragment{

    View myView;
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    public FirstFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.first_layout, container, false);
        // get the listview
        expListView = (ExpandableListView) myView.findViewById(R.id.lvExp);

        // preparing list data
        prepareListData();

        return myView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    /*
     * Preparing the list data
     */
    private void prepareListData() {
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();


        // Adding child data
        listDataHeader.add("Introductie");
        listDataHeader.add("Week 1");
        listDataHeader.add("Week 2");
        listDataHeader.add("Week 3");
        listDataHeader.add("Week 4");
        listDataHeader.add("Week 5");
        listDataHeader.add("Week 6");
        listDataHeader.add("Week 7");

        // Adding child data
        List<String> intro = new ArrayList<String>();
        intro.add("Intro");

        // Adding child data
        List<String> week1 = new ArrayList<String>();
        week1.add("Voorbereiding labjournaal 1");
        week1.add("Labjournaal 1");
        week1.add("Praktijk 1");
        week1.add("Micropipetten");
        week1.add("Gebruik micropipet 1");
        week1.add("Gebruik micropipet 2");
        week1.add("Gebruik micropipet 3");
        week1.add("Practicum 1");
        week1.add("Info praktijk 2");
        week1.add("Practicum 2");
        week1.add("Voorbereiding PT3");

        List<String> week2 = new ArrayList<String>();


        List<String> week3 = new ArrayList<String>();


        List<String> week4 = new ArrayList<String>();


        List<String> week5 = new ArrayList<String>();


        List<String> week6 = new ArrayList<String>();


        List<String> week7 = new ArrayList<String>();


        listDataChild.put(listDataHeader.get(0), intro); // Header, Child data
        listDataChild.put(listDataHeader.get(1), week1);
        listDataChild.put(listDataHeader.get(2), week2);
        listDataChild.put(listDataHeader.get(3), week3);
        listDataChild.put(listDataHeader.get(4), week4);
        listDataChild.put(listDataHeader.get(5), week5);
        listDataChild.put(listDataHeader.get(6), week6);
        listDataChild.put(listDataHeader.get(7), week7);

        listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);


        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {


                // TODO Auto-generated method stub
                final String selected = (String) listAdapter.getChild(
                        groupPosition, childPosition);

                Intent intent;
                switch(selected) {
                    case "Intro":
                        intent = new Intent(getActivity(), Introductie.class);
                        intent.putExtra("week","1");
                        intent.putExtra("deel","introductie");
                        startActivity(intent);
                        break;

                    case "Voorbereiding labjournaal 1":
                        intent = new Intent(getActivity(), Introductie.class);
                        intent.putExtra("week","1");
                        intent.putExtra("deel","Voorbereiding labjournaal 1");
                        startActivity(intent);
                        break;
                    case "Labjournaal 1":
                        intent = new Intent(getActivity(), Introductie.class);
                        intent.putExtra("week","1");
                        intent.putExtra("deel","Labjournaal 1");
                        startActivity(intent);
                        break;
                    case "Praktijk 1":
                        intent = new Intent(getActivity(), Introductie.class);
                        intent.putExtra("week","1");
                        intent.putExtra("deel","Praktijk 1");
                        startActivity(intent);
                        break;
                    case "Micropipetten":
                        intent = new Intent(getActivity(), Introductie.class);
                        intent.putExtra("week","1");
                        intent.putExtra("deel","Micropipetten");
                        startActivity(intent);
                        break;
                    case "Gebruik micropipet 1":
                        intent = new Intent(getActivity(), Introductie.class);
                        intent.putExtra("week","1");
                        intent.putExtra("deel","Gebruik micropipet 1");
                        startActivity(intent);
                        break;
                    case "Gebruik micropipet 2":
                        intent = new Intent(getActivity(), Introductie.class);
                        intent.putExtra("week","1");
                        intent.putExtra("deel","Gebruik micropipet 2");
                        startActivity(intent);
                        break;
                    case "Gebruik micropipet 3":
                        intent = new Intent(getActivity(), Introductie.class);
                        intent.putExtra("week","1");
                        intent.putExtra("deel","Gebruik micropipet 3");
                        startActivity(intent);
                        break;
                    case "Practicum 1":
                        intent = new Intent(getActivity(), Introductie.class);
                        intent.putExtra("week","1");
                        intent.putExtra("deel","Practicum 1");
                        startActivity(intent);
                        break;
                    case "Info praktijk 2":
                        intent = new Intent(getActivity(), Introductie.class);
                        intent.putExtra("week","1");
                        intent.putExtra("deel","Info praktijk 2");
                        startActivity(intent);
                        break;
                    case "Practicum 2":
                        intent = new Intent(getActivity(), Introductie.class);
                        intent.putExtra("week","1");
                        intent.putExtra("deel","Practicum 2");
                        startActivity(intent);
                        break;
                    case "Voorbereiding PT3":
                        intent = new Intent(getActivity(), Introductie.class);
                        intent.putExtra("week","1");
                        intent.putExtra("deel","Voorbereiding PT3 1");
                        startActivity(intent);
                        break;
                }

                Snackbar.make(
                        getView(),
                        listDataHeader.get(groupPosition)
                                + " : "
                                + listDataChild.get(
                                listDataHeader.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT)
                        .show();
                return false;

            }

        });

    }
}
